import time
import csv
import json
import datetime
import os
import os.path

file = "_3_Filipe-2015-07-29-2015-08-05.csv"
EPOCH_TIME= 0
STRING_TIME= 1
POWER= 2
AMPLITUDE_PERCENTAGE = 0.27
MINIMIM_AMPLITUDE = 6
WINDOW_SIZE = 5
SKIP_SIZE = 3

def eraseFile(filename):
    f = open(filename, "w")
    f.close()

def getHeaders():
    return "powerIni,powerFinal,EPOCH_TIME,STRING_TIME,medianDifference,stateDifference,changeType\n"

def yieldArrayFromFile(fileList):
    """
    Reads values stored in a CSV file and returns them in an array of arrays
    :return: array of arrays with each line and csv values
    """
    try:
        for file in fileList:
            if not os.path.isfile(file):
                continue
            with open(file, 'r') as f:
                for row in f:
                    yield row.replace('\r', '').replace('\n', '').split(',')    # returns a list without \n and \r assuming each value is a CSV line
        return
    except:   # error reading file
        return

def minMaxPowerDynamic(fileList, index=0):
    gen = yieldArrayFromFile(fileList)
    if(gen == False): return 0,0

    min = gen.next()
    min = float(min[index])
    max = min
    for array in gen:
        if(float(array[index]) < min):
            min = float(array[index])
        if(float(array[index]) > max):
            max = float(array[index])
    return min, max

def getMiddleIndex(size):
	result = ((size +1)/2)-1
	if result < 0:
		return 0
	return result

def median(lst):
    lst = sorted(lst)
    if len(lst) < 1:
        return None
    if len(lst) %2 == 1:    # there is a middle value (len(lst) is odd)
        return lst[((len(lst)+1)/2)-1]
    else:
        return float(sum(lst[(len(lst)/2)-1:(len(lst)/2)+1]))/2.0

class SlidingWindow():
    def __init__(self, windowSize):
        self.maxSize = windowSize
        self.items = []

    def append(self, item):
        if(len(self.items) >= self.maxSize):
            del self.items[0]
        self.items.append(item)

    def currentSize(self):
        return len(self.items)

def generateReportFromGenerator(gen, medianThreshold):
    """
    :param filename: name of the file where the logs are stored
    :return: a csv formated string with states separated by lines
    """
    stateResult = ""
    slidingWindow = SlidingWindow(WINDOW_SIZE)
    #gen.next() # to skip the mac address in the first line

    skip = 0

    for array in gen:
    	powerDict = {"power": float(array[POWER]),"epochTime":array[EPOCH_TIME], "stringTime":array[STRING_TIME]}

        slidingWindow.append(powerDict)    # adds power to the sliding window
        if(slidingWindow.currentSize() < WINDOW_SIZE):   # keeps the sliding window
            previousMedian = powerDict
            previousState  = powerDict

        slidingMedian = median([item["power"] for item in slidingWindow.items])
        medianDifference = abs(slidingMedian - previousMedian['power'])
        stateDifference  = abs(slidingMedian - previousState['power'])

        if skip > 0:    # skip samples because an event was just detected (to not detect twice)
            skip -= 1
            previousState = {"power":slidingMedian, "epochTime":powerDict["epochTime"], "stringTime":powerDict["stringTime"]}
            previousMedian = {"power":slidingMedian, "epochTime":powerDict["epochTime"], "stringTime":powerDict["stringTime"]}
            continue

        if(medianDifference > medianThreshold) or (stateDifference > medianThreshold):
            skip = SKIP_SIZE
            powerIni = slidingWindow.items[0]["power"]
            powerFinal = slidingMedian

            middleIndex = getMiddleIndex(slidingWindow.currentSize())

            # check change type
            if(medianDifference > medianThreshold):
                changeType = "down" if powerIni > powerFinal else "up"
            elif(stateDifference > medianThreshold):
                changeType = "downGradually" if powerIni > powerFinal else "upGradually"

            stateResult += (str(powerIni)
                            + "," + str(powerFinal)
                            + "," + slidingWindow.items[middleIndex]['epochTime']
                            + "," + slidingWindow.items[middleIndex]['stringTime']
                            + "," + str(medianDifference)
                            + "," + str(stateDifference)
                            + "," + changeType
                            + "\n")
            previousState = {"power":slidingMedian, "epochTime":powerDict["epochTime"], "stringTime":powerDict["stringTime"]}
        previousMedian = {"power":slidingMedian, "epochTime":powerDict["epochTime"], "stringTime":powerDict["stringTime"]}
    return stateResult

def generateReport(fileList):
    min, max = minMaxPowerDynamic(fileList, POWER)
    amplitude = max - min
    print("MIN; MAX", min, max)
    print MINIMIM_AMPLITUDE, amplitude
    if amplitude < MINIMIM_AMPLITUDE:   # amplitude(w) too low, there wasn't any power changes/consumed, return empty state string
        print ("amplitude too low",amplitude)
        return ""
    stateThreshold = amplitude * AMPLITUDE_PERCENTAGE

    fileGen = yieldArrayFromFile(fileList)
    return generateReportFromGenerator(fileGen, stateThreshold)

def  writeToFile(data, filename):
    f = open(filename, "w")
    f.write(data)
    f.close()

if(__name__ == "__main__"):
    report_filename = "report-" + file
    eraseFile(report_filename)
    data = generateReport(file)
    writeToFile(getHeaders() + data, report_filename)
