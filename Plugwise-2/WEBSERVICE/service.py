#!/usr/bin/env python
from flask import Flask, jsonify, request, Response, url_for, send_from_directory
import time
import json
import datetime
import os
import os.path

import transition_report
#from flask.ext.cors import CORS, cross_origin

PW_CONTROL_FILE = "../config/pw-control.json"
PW_CONF_FILE = "../config/pw-conf.json"
app = Flask(__name__)
#app.config['SERVER_NAME']="plug"
#cors = CORS(app)

#@cross_origin() # allow all origins all methods.
@app.route('/', methods=['GET', 'POST'])
@app.route('/<path:file_name>', methods=['GET', 'POST'])
def index(file_name="index.html"):
    """
        Displays current data from the plugs (not auto updated)
        put JS event to call for mac and update its present current
    """
    return send_from_directory('www', filename=file_name)

@app.route('/css/<path:file_name>/', methods=['GET', 'POST'])
def css(file_name):
    return send_from_directory('www/css', filename=file_name)

@app.route('/img/<path:file_name>/', methods=['GET', 'POST'])
def img(file_name):
    return send_from_directory('www/img', filename=file_name)

@app.route('/js/<path:file_name>/', methods=['GET', 'POST'])
def js(file_name):
    return send_from_directory('www/js', filename=file_name)

@app.route('/js/highcharts/<path:file_name>/', methods=['GET', 'POST'])
def js_highcharts(file_name):
    return send_from_directory('www/js/highcharts', filename=file_name)

@app.route('/js/highcharts/modules/<path:file_name>/', methods=['GET', 'POST'])
def js_highcharts_modules(file_name):
    return send_from_directory('www/js/highcharts/modules', filename=file_name)

@app.route('/chart', methods=['GET', 'POST'])
@app.route('/chart/<path:mac_address>/<path:startDateS>', methods=['GET', 'POST'])
@app.route('/chart/<path:mac_address>/<path:startDateS>/<path:endDateS>', methods=['GET', 'POST'])
def chart(mac_address, startDateS="none", endDateS="none"):
    return send_from_directory('www', filename="chart.html")

@app.route('/chart_high', methods=['GET', 'POST'])
@app.route('/chart_high/<path:mac_address>/<path:startDateS>', methods=['GET', 'POST'])
@app.route('/chart_high/<path:mac_address>/<path:startDateS>/<path:endDateS>', methods=['GET', 'POST'])
def chart_high(mac_address, startDateS="none", endDateS="none"):
    return send_from_directory('www', filename="chart_high.html")

#@cross_origin() # allow all origins all methods.
@app.route('/get_file/<path:mac_address>', methods=['GET'])
@app.route('/get_file/<path:mac_address>/<path:jsonReturn>', methods=['GET'])
@app.route('/get_file/<path:mac_address>/<path:jsonReturn>/<path:startDateS>', methods=['GET'])
@app.route('/get_file/<path:mac_address>/<path:jsonReturn>/<path:startDateS>/<path:endDateS>', methods=['GET'])
def get_file(mac_address, jsonReturn="csv", startDateS="none", endDateS="none"):
    """
        Displays current data from the plugs (not auto updated)
        put JS event to call for mac and update its present current
    """
    if (startDateS == "none"):
        return "no start date specifyed"
    if (endDateS == "none"):
        endDateS = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d')

    startDateS = startDateS.replace('/', '-')
    endDateS   = endDateS.replace('/', '-')
    start_date = datetime.datetime.strptime(startDateS, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(endDateS, "%Y-%m-%d")
    nr_days = (end_date - start_date).days
    nr_days += 1 	# to include the day of endDate
    return_data = ""

    for _ in range(nr_days):
        int_date = time.mktime(start_date.timetuple())  # transform datetime into time
        str_date = datetime.datetime.fromtimestamp(int_date).strftime('%Y-%m-%d')   # transform time into str
        path = "/RESULTS/actual/" + str(start_date.year) + "/pwact/pwact-" + str_date + '-' + mac_address + '.log'

        # check if file exists
        if os.path.isfile(path):
            with open(path, 'r') as f:
                return_data += f.read()

        date_plus_day = start_date.date() + datetime.timedelta(days=1)
        start_date = datetime.datetime.strptime(str(date_plus_day), "%Y-%m-%d")
        #start_date = start_date.date() + datetime.timedelta(days=1)

    # find plug name
    plug_name = mac_address
    f = open(PW_CONTROL_FILE, 'r')
    data = json.load(f)
    f.close()
    for value in data['dynamic']:
        if(value['mac'] == mac_address):
            plug_name = value['name']

    if jsonReturn.lower() == "true" or jsonReturn.lower() == "json":
        jsonData = {"mac": mac_address, "name":plug_name, "data":[]}
        split_data = return_data.split('\n')
        if split_data[len(split_data) -1] == ['']: del split_data[len(split_data) - 1]
        for line in split_data:
            split_line = line.split(',')
            try:
                jsonData['data'].append({"epoch": split_line[0], "string_time":split_line[1], "power":split_line[2]})
            except: print split_line
        return json.dumps(jsonData)
    else:
        response = Response(return_data, mimetype='text/csv')
        response.headers['Content-Disposition'] = 'attachment; filename=' + plug_name + '-' + mac_address + '-' + startDateS + '-' + endDateS + '.csv'
        return response

@app.route('/get_state_changes/<path:mac_address>', methods=['GET'])
@app.route('/get_state_changes/<path:mac_address>/<path:jsonReturn>', methods=['GET'])
@app.route('/get_state_changes/<path:mac_address>/<path:jsonReturn>/<path:startDateS>', methods=['GET'])
@app.route('/get_state_changes/<path:mac_address>/<path:jsonReturn>/<path:startDateS>/<path:endDateS>', methods=['GET'])
def get_state_changes(mac_address, jsonReturn="true",startDateS="none", endDateS="none"):
    """
        Displays current data from the plugs (not auto updated)
        put JS event to call for mac and update its present current
    """
    if (startDateS == "none"):
        return "no start date specifyed"
    if (endDateS == "none"):
        endDateS = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d')

    startDateS = startDateS.replace('/', '-')
    endDateS   = endDateS.replace('/', '-')
    start_date = datetime.datetime.strptime(startDateS, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(endDateS, "%Y-%m-%d")
    nr_days = (end_date - start_date).days
    nr_days += 1 	# to include the day of endDate
    return_data = ""
    fileList = []
    for _ in range(nr_days):
        int_date = time.mktime(start_date.timetuple())  # transform datetime into time
        str_date = datetime.datetime.fromtimestamp(int_date).strftime('%Y-%m-%d')   # transform time into str
        path = "/RESULTS/actual/" + str(start_date.year) + "/pwact/pwact-" + str_date + '-' + mac_address + '.log'

        fileList.append(path)
        # check if file exists
        #print path
        date_plus_day = start_date.date() + datetime.timedelta(days=1)
        start_date = datetime.datetime.strptime(str(date_plus_day), "%Y-%m-%d")
    return_data += transition_report.generateReport(fileList)

    plug_name = mac_address
    f = open(PW_CONTROL_FILE, 'r')
    pw_data = json.load(f)
    f.close()
    for value in pw_data['dynamic']:
        if(value['mac'] == mac_address):
            plug_name = value['name']

    if(jsonReturn.lower() == "json" or jsonReturn.lower() == "true"):   #return changes in JSON
        keys = ("powerIni","powerFinal", "epochTime", "stringTime", "medianDifference", "stateDifference", "", "changeType")
        jsonData = {'name':plug_name,"state_changes":[]}
        return_data = return_data.split('\n')
        for line in return_data:
            line = line.split(',')
            nr_keys = len(keys) if len(keys) < len(line) else len(line)
            # print line
            #print nr_keys
            dict = {}
            for i in range(nr_keys):
                dict[keys[i]] = line[i]

            jsonData['state_changes'].append(dict)
        return json.dumps(jsonData)
    else:   # return changes in file
        return_data = mac_address + "\n" + return_data
        response = Response(return_data, mimetype='text/csv')
        response.headers['Content-Disposition'] = 'attachment; filename=report-' + plug_name + '-' + startDateS + '-' + endDateS + '.csv'
        return response

#@cross_origin() # allow all origins all methods.
@app.route('/get_current', methods=['GET'])
def get_current():
    """
        Displays current data from the plugs (not auto updated)
        put JS event to call for mac and update its present current
    """

    # getting MACs
    f = open(PW_CONTROL_FILE, 'r')
    data = json.load(f)
    f.close()
    mac_dic = {}
    for value in data['dynamic']:
        mac_dic[value['mac']] = {'name':value['name'], 'monitor':value['monitor']}	#turns into list

    # getting actual consumption
    filesize = os.stat('/RESULTS/actual/pwpower.log').st_size	#this usually guarantess the file has contents, as it only returns after the file os closed for writing
    if filesize == 0: return "bkabka"
    f = open("/RESULTS/actual/pwpower.log", 'r')
    for row in f:
        row_list = row.replace(' ','').replace('\n','').replace('\r','').split(',')
        mac = row_list[0]
        consumption = row_list[1]
        mac_dic[mac]['consumption'] = consumption
    f.close()

    return json.dumps(mac_dic)

@app.route('/upload_name/<path:old_name>/<path:new_name>', methods=['GET'])
def upload_name(old_name="", new_name=""):
    """
        Displays current data from the plugs (not auto updated)
        put JS event to call for mac and update its present current
    """
    if old_name == "" or new_name == "":
        return "False"
    try:
        fstatic = open(PW_CONF_FILE)
        rstatic = fstatic.read()
        fstatic.close()
        rstatic = rstatic.replace(old_name, new_name, 1)
        fstatic = open(PW_CONF_FILE, "w")
        print rstatic
        fstatic.write(rstatic)
        fstatic.close()
    except:
        print "ERROR changing name in pw-conf.json"
    try:
        fdynamic = open(PW_CONTROL_FILE)
        rdynamic = fdynamic.read()
        fdynamic.close()
        rdynamic = rdynamic.replace(old_name, new_name, 1)
        fdynamic = open(PW_CONTROL_FILE, "w")
        fdynamic.write(rdynamic)
        print rdynamic
        fdynamic.close()
    except: print "ERROR changing name in pw-control.json"
    return "OK"

@app.route('/change_monitor_state/<path:mac>/<path:state>', methods=['GET'])
def change_monitor_state(mac="", state=""):
    """
        Displays current data from the plugs (not auto updated)
        put JS event to call for mac and update its present current
    """
    if mac == "" or state == "":
        return "False"

    try:
        with open(PW_CONTROL_FILE) as file:
            contents = json.load(file)
            for plug in contents['dynamic']:
                if plug['mac'] == mac:
                    plug['monitor'] = state
        with open(PW_CONTROL_FILE, "w") as file:
            json.dump(contents, file)
    except: print "ERROR changing name2"
    return "OK"

app.run(debug = True, port=80, host='0.0.0.0')
